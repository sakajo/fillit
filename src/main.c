/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <jfazakas@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 20:03:29 by jfazakas          #+#    #+#             */
/*   Updated: 2016/01/09 20:32:36 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int ac, char **av)
{
	t_tetris	*list;

	if (ac == 2)
	{
		list = get_tetriminos_list(av[1]);
		fillit(list);
	}
	else
		print_message_and_exit("usage: ./fillit source_file\n", 1);
	return (0);
}
