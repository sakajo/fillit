/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <jfazakas@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 20:57:18 by jfazakas          #+#    #+#             */
/*   Updated: 2016/01/09 20:58:14 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

unsigned char	get_map_size(t_tetris *list)
{
	unsigned char	pieces;
	unsigned char	size;
	t_tetris		*start;

	pieces = 0;
	start = list;
	while (start)
	{
		pieces++;
		start = start->next;
	}
	size = 0;
	while (size * size < pieces * 4)
		size++;
	return (size);
}
