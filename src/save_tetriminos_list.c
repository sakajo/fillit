/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save_tetriminos_list.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <jfazakas@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 20:38:10 by jfazakas          #+#    #+#             */
/*   Updated: 2016/01/09 21:14:20 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	save_tetriminos_list(t_tetris **tetriminos, int fd)
{
	char			**new_piece;
	char			*line;
	unsigned char	*new_coordinates;
	char			letter;
	int				boolean;

	boolean = 1;
	letter = 'A';
	while (boolean == 1 && letter <= 'Z')
	{
		new_piece = get_piece(fd);
		new_coordinates = get_coordinates(new_piece);
		adjust_coordinates(new_coordinates);
		add_piece_to_list(tetriminos, new_coordinates, letter);
		if (get_next_line(fd, &line) == 0)
			boolean = 0;
		else if (!ft_strequ(line, ""))
			print_message_and_exit("error\n", 2);
		letter++;
	}
	if (get_next_line(fd, &line) > 0)
		print_message_and_exit("error\n", 2);
}
