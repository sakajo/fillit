/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <jfazakas@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/08 19:41:42 by jfazakas          #+#    #+#             */
/*   Updated: 2016/01/09 20:58:23 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void					fillit(t_tetris *list)
{
	char				*map;
	unsigned char		size;

	size = get_map_size(list);
	change_coordinates(list, size);
	while (1)
	{
		map = create_map(size);
		backtrack(list, map);
		size++;
		increment_coordinates(list, size);
	}
}
