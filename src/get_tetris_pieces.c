/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_tetris_pieces.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <jfazakas@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 20:01:54 by jfazakas          #+#    #+#             */
/*   Updated: 2016/01/09 20:31:53 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_tetris	*get_tetriminos_list(char *filename)
{
	t_tetris	*tetriminos;
	int			fd;

	fd = open(filename, O_RDONLY);
	if (fd == -1)
		print_message_and_exit("error\n", 2);
	tetriminos = NULL;
	save_tetriminos_list(&tetriminos, fd);
	return (tetriminos);
}
