/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <jfazakas@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/09 20:58:42 by jfazakas          #+#    #+#             */
/*   Updated: 2016/01/09 21:03:11 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char				*create_map(unsigned char size)
{
	char	*map;
	int		index;

	map = (char*)malloc(sizeof(char) * ((size + 1) * (size + 1)));
	index = 0;
	while (index < size)
	{
		ft_memset(map + index * (size + 1), '.', size);
		map[(index + 1) * (size + 1) - 1] = '\n';
		index++;
	}
	ft_memset(map + index * (size + 1), '\0', size + 1);
	return (map);
}
