# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/04 15:16:36 by jfazakas          #+#    #+#              #
#    Updated: 2016/01/09 21:47:20 by jfazakas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit

SRC = src/main.c\
	  src/add_piece_to_list.c\
	  src/adjust_coordinates.c\
	  src/backtrack.c\
	  src/change_coordinates.c\
	  src/create_map.c\
	  src/fillit.c\
	  src/get_coordinates.c\
	  src/get_map_size.c\
	  src/get_piece.c\
	  src/get_tetris_pieces.c\
	  src/increment_coordinates.c\
	  src/print_message_and_exit.c\
	  src/save_tetriminos_list.c\

OBJ = main.o\
	  add_piece_to_list.o\
	  adjust_coordinates.o\
	  backtrack.o\
	  change_coordinates.o\
	  create_map.o\
	  fillit.o\
	  get_coordinates.o\
	  get_map_size.o\
	  get_piece.o\
	  get_tetris_pieces.o\
	  increment_coordinates.o\
	  print_message_and_exit.o\
	  save_tetriminos_list.o\

INC = includes/

FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	make -C libft
	gcc $(FLAGS) -I $(INC) -c $(SRC)
	gcc $(OBJ) -L libft/ -lft -o $(NAME)

clean:
	make -C libft/ clean
	rm -f $(OBJ)

fclean: clean
	make -C libft/ fclean
	rm -f $(NAME)

re: fclean all
